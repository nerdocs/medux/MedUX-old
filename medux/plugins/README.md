# Plugins

This directory contains django apps as MedUX plugins.
Just add any Django app here (you can create one with `./manage.py startplugin`).
If you add them to INSTALLED_APPS directly ('medux.plugins.fooplugin'),
it will be loaded as normal Django
app, or you could make an installable package using setup_tools and use it as
Python egg. 
