"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from channels.generic.websocket import AsyncWebsocketConsumer
import json


class ResourceConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.resource_name = self.scope["url_route"]["kwargs"]["resource_name"]
        self.resource_group_name = "resource_%s" % self.resource_name
        await self.channel_layer.group_add(self.resource_group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.resource_group_name, self.channel_name
        )

    async def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]

        await self.channel_layer.group_send(
            self.resource_group_name, {"type": "resource_message", "message": message}
        )

    async def resource_message(self, event):
        message = event["message"]

        await self.send(text_data=json.dumps({"message": message}))
