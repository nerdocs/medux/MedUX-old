"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models

from gdaps.validators import validate_plugin_archive_fileextension


PluginNameValidator = RegexValidator(
    r"^[a-z\-_]+", "Only lowercase letters, " "hyphens and underscores allowed."
)


class MeduxPlugin(models.Model):

    installed = models.BooleanField(default=False)
    name = models.CharField(
        max_length=10, unique=True, validators=[PluginNameValidator]
    )
    app_name = models.CharField(max_length=255)
    verbose_name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    version = models.CharField(max_length=30)
    core_compat_version = models.CharField(max_length=30)

    category = models.CharField(max_length=255, blank=True)
    author = models.CharField(max_length=255)
    author_email = models.EmailField(blank=True)
    vendor = models.CharField(max_length=255, blank=True)
    archive = models.FileField(
        upload_to="", validators=[validate_plugin_archive_fileextension]
    )

    activated = models.BooleanField(default=False)

    dependencies = models.ManyToManyField(
        "MeduxPlugin",
        related_name="dependants",
        verbose_name="other plugins that " "this plugin depends " "on.",
    )

    def initialize(self):
        """Basic module initialization at first run."""

    def activate(self):
        """Activate module"""
        self.activated = True
        self.save()

    def deactivate(self):
        """Deactivate module"""
        self.activated = False
        self.save()

    def update_from_spec(self, spec: dict) -> None:
        plugin_spec = spec["Plugin"]

        self.name = plugin_spec["name"]
        self.app_name = plugin_spec["app_name"]
        self.verbose_name = plugin_spec["verbose_name"]
        self.description = plugin_spec["description"]
        self.vendor = plugin_spec["vendor"]
        self.version = str(plugin_spec["version"])
        #        self.core_compat_version = str(
        #            plugin_spec['dependencies']['core'])
        self.author = plugin_spec["author"]
        self.category = plugin_spec["category"] or "Misc"
        self.author_email = plugin_spec["author_email"]
        # self.dependencies = plugin_spec['dependencies'] or []
        self.save()

    def on_uninstall(self):
        """called on removal of the module..

        This should ideally remove all traces from the database,
        """

    def __str__(self):
        return self.verbose_name
