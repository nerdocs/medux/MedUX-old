# MedUX Base System

This is the MedUX base system. It provides a basic software architecture for
the MedUX EMR:

  * Extension system for plugins with a plugin loader
  * Django server
  * Django channels
  * REST service API
  * GraphQL API

Plugins can extend MedUX - even the Medux "core" is a plugin.