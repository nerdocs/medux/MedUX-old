"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, re_path, include
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from medux.schema import schema
from gdaps.pluginmanager import PluginManager
from graphene_django.views import GraphQLView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

# TODO make MedUX translatable:
# https://stackoverflow.com/questions/20467626/how-to-setup-up-django-translation-in-the-correct-way

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/token-auth/", obtain_jwt_token),
    path("api/token-refresh/", refresh_jwt_token),
    path("graphql/", csrf_exempt(GraphQLView.as_view(graphiql=True, schema=schema))),
    # path('plugins/', PluginAPIView.as_view(), name='plugin-api'),
]

# URLs from <plugins>.urls.py are included automatically.
# This is maybe not the best approach, but it allows plugins to
# have "global" URLs, and not only namespaced, and it is flexible

urlpatterns += PluginManager.urlpatterns()


# all other URLs should be interpreted by the frontend
# urlpatterns += [
#     re_path(r'^(.*/)*$', TemplateView.as_view(template_name='index.html'),
#             name='index'),
# ]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
