"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from medux.core.models import MeduxModel, PolymorphicModel


class MaritalStatus(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = _("Marital statuses")


class GenderLabel(models.Model):
    tag = models.CharField(primary_key=True, max_length=2)
    label = models.CharField(max_length=50)
    sort_weight = models.IntegerField(default=0)
    comment = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.label


class Country(models.Model):
    """Countries coded per ISO 3166-1

    see https://en.wikipedia.org/wiki/ISO_3166-1"""

    code = models.CharField(max_length=2, primary_key=True)
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = _("Countries")

    def __str__(self):
        return self.name


class AddressType(models.Model):
    """Type of an address, like home, work, parents, holidays etc."""

    name = models.CharField(max_length=255)


class Address(MeduxModel):
    street = models.CharField(max_length=255, blank=True)

    # additional street-level information which formatters would usually
    # put on lines directly below the street line of an address, such as
    # postal box directions in CA, or c/o hints
    aux_street = models.CharField(max_length=255, blank=True)

    number = models.CharField(max_length=50, blank=True)

    # directions *below* the unit (eg.number) level, such as apartment number,
    # room number, level, entrance or even verbal directions
    subunit = models.CharField(max_length=255, blank=True)

    postcode = models.CharField(max_length=10, blank=True)

    city = models.CharField(max_length=255, blank=True)

    state = models.ForeignKey(Country, on_delete=models.PROTECT)
    # any additional information that did not fit anywhere else
    addendum = models.CharField(max_length=255, blank=True)

    # the exact location of this address in latitude-longtitude
    location = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name_plural = _("Addresses")


class IdentityAddressMapper(models.Model):
    """a many-to-many pivot table

    It describes the relationship between an organisation, a person,
    their work address and their occupation at that location.
    For patients id_org is NULL"""

    identity = models.ForeignKey("Identity", on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    type = models.ForeignKey(AddressType, on_delete=models.PROTECT)


class Identity(MeduxModel):
    pupic = models.CharField(
        max_length=24,
        blank=True,
        null=True,
        help_text=_(
            "Portable Unique Person "
            "Identification Code as per GNUmed "
            "white papers"
        ),
    )  # Todo: implement
    # generatePubic and set required=True

    gender = models.ForeignKey(GenderLabel, on_delete=models.PROTECT)

    dob = models.DateTimeField(verbose_name=_("Date/time of birth"))
    dob_is_estimated = models.BooleanField(default=False)

    marital_status = models.ForeignKey(
        MaritalStatus, null=True, blank=True, on_delete=models.SET_NULL
    )

    # country of birth as per date of birth, coded as 2 character ISO code
    cob = models.ForeignKey(
        Country,
        help_text=_("ISO code of Country of Birth"),
        on_delete=models.SET_NULL,
        null=True,
    )
    # TODO: make choices=ISO3166 fixture

    # date when a person has died
    deceased = models.DateTimeField(
        verbose_name=_("Date/Time of death"), blank=True, null=True
    )

    title = models.CharField(max_length=50, blank=True, null=True)

    addresses = models.ManyToManyField(Address, through="IdentityAddressMapper")

    # Free text emergency contact information.
    emergency_contact_freetext = models.CharField(max_length=255, blank=True, null=True)

    # Link to another Identity to be used as emergency contact
    emergency_contact = models.ForeignKey(
        "Identity", null=True, blank=True, on_delete=models.SET_NULL
    )

    comment = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="""A free-text
        comment on this identity.\n\n
        Can be used to to discriminate patients which are otherwise\n
        identical regarding name and date of birth.\n
        Should be something non-ephemereal and unique to the person\n
        itself across time, place and database instance.\n
        Good: place of birth\n
        Good: maiden name of mother\n
        Good: name of mother\n
        Good: hash of DNA\n
        Bad: current address (will change)\n
        Bad: primary provider in this practice (can be invalid in another)\n
        Bad: nickname (will change, can dupe as well)\n Bad: favourite food\n
        not-quite-so-bad: occupation""",
    )

    def generate_pupic(self):
        """Generates a Portable Unique Person Identification according to
        GNUmed definition

        See http://wiki.gnumed.de/bin/view/Gnumed/PatientIdentity"""
        # TODO: implement PUPIC generation
        return ""

    class Meta:
        verbose_name_plural = _("Identities")

    def __str__(self):
        return self.pupic or self.dob


class HealthServiceProvider(PolymorphicModel):
    """A base class for Practitioner, and other Health service providers."""

    name = models.CharField(max_length=255)
    address = models.ForeignKey(Address, on_delete=models.PROTECT)


class Practitioner(HealthServiceProvider):
    pass


class Organization(HealthServiceProvider):
    class Meta:
        abstract = True


class Relation(models.Model):
    """A relation between 2 identities.

    This could be father, mother, son, daughter, uncle, friend etc."""

    name = models.CharField(max_length=255)


class Patient(MeduxModel):
    identity = models.ForeignKey(Identity, on_delete=models.CASCADE)
    active = models.BooleanField(
        help_text=_("Whether this patient record is in active use."), default=True
    )
    general_practitioner = models.ManyToManyField(HealthServiceProvider, blank=True)
    related_persons = models.ManyToManyField("Patient", through="RelatedPerson")

    def __str__(self):
        return self.name()

    def name(self):
        return self.identity.names.first() or "---"


class RelatedPerson(models.Model):
    one = models.ForeignKey(Patient, on_delete=models.CASCADE, related_name="+")
    other = models.ForeignKey(Patient, on_delete=models.CASCADE, related_name="+")
    relation_type = models.ForeignKey(Relation, on_delete=models.PROTECT)


class Name(models.Model):
    """All the names an identity is known under"""

    # As opposed to the versioning of all other tables,
    # changed names should not be soft-deleted.
    # Search functionality must be available at any time for all names a
    # person ever had.

    # FIXME: use auditing functionality here too. created/modified.
    # Maybe split the MeduxModel into two: auditing and soft-deletion?

    identity = models.ForeignKey(
        Identity, related_name="names", on_delete=models.CASCADE
    )
    lastname = models.CharField(max_length=255)
    firstname = models.CharField(max_length=255)
    preferred = models.CharField(
        max_length=255,
        help_text=_(
            """
        preferred first name, the name a person is usually
        called (nickname)"""
        ),
        blank=True,
        null=True,
    )
    comment = models.CharField(
        max_length=255,
        help_text=_(
            """a comment regarding this name,
         useful in things like "this was the name before marriage" etc."""
        ),
        blank=True,
        null=True,
    )

    def __str__(self):
        # TODO: make order configurable
        return "{}, {}".format(self.lastname, self.firstname)


# TODO: return current user.
def current_user():
    # stub
    return 0


class Encounter(MeduxModel):
    """A clinical encounter between a person and the health care system."""

    date = models.DateTimeField(default=timezone.now)

    # TODO: add a function to set this to an "anonymous" in case of deletion
    provider = models.ForeignKey(
        HealthServiceProvider, on_delete=models.PROTECT, default=current_user
    )


class Problem(MeduxModel):
    """Represents a clinical problem during a time.

    In other specifications, this could be named health issue, condition
    (FHIR), diagnosis etc.
    One or more (ICPC2/ICD10 coded) diagnoses could be a part of a problem,
    but there could be problems that have no clear diagnosis as well.
    """

    name = models.CharField(max_length=255)


class NarrativeType(models.Model):
    """The type of a narrative.

    This per default are the first 2 letters of narratives in the SOAP schema:
    S subjective, anamnesis
    O objective, findings
    A assessment, differential diagnoses
    P plan - what to do next
    """

    shortcut = models.CharField(primary_key=True, max_length=2)
    name = models.CharField(max_length=100)


class Narrative(MeduxModel):
    encounter = models.ForeignKey(Encounter, on_delete=models.CASCADE)

    problem = models.ForeignKey(Problem, on_delete=models.PROTECT)
    narrative = models.TextField()

    type = models.ForeignKey(NarrativeType, on_delete=models.PROTECT)
