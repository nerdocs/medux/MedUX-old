"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = "Christian González <christian.gonzalez@nerdocs.at>"

# from .fhir_elements import *
# from .fhir_resources import *
from .core import *
from .navigation import *
from .patient import *

# The FHIR specification defines a set of data types that are used for the
# resource elements. There are four categories of data types, implemented
# partly as Django models, Partly as Django fields:
#
# *   Simple / primitive types, which are single elements with a primitive
#     value (see fields.py)
# *   General purpose complex types, which are re-usable clusters of
#     elements (see elements.py)
# *   Metadata types: A set of types for use with metadata resources
#     (see resources.py)
# *   Special purpose data types - defined elsewhere in the specification
#     for specific usages (see resources.py)
