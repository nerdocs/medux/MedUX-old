import uuid as uuid
from django.contrib.auth.models import Permission
from django.db import models
from django.utils.translation import gettext_lazy as _


class MenuItem(models.Model):
    """A serializable menu item, for displaying in a menu"""

    menu = models.BooleanField(default=False, help_text=_("This menu item is a menu."))

    parent = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        related_name="%(class)s_parent",
        on_delete=models.CASCADE,
        verbose_name=_("Parent menu item"),
        db_index=True,
    )
    app = models.CharField(
        max_length=255, help_text="The app in which the menu item belongs to."
    )
    title = models.CharField(
        max_length=60,
        help_text="The title, as displayed to the user. A '&' character precedes the "
        "short-cut character, like '&New'.",
    )
    slug = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("The slug of the page you want to link to."),
    )
    weight = models.SmallIntegerField(
        default=0,
        help_text='The "weight" of the menu element. '
        "Negative numbers are lighter, positive are heavier. "
        "Heavier elements sink down in the list.",
    )
    checkable = models.BooleanField(default=False)
    checked = models.BooleanField(default=False)
    disabled = models.BooleanField(default=False)
    icon = models.CharField(max_length=60, blank=True, null=True)
    access_permissions = models.ManyToManyField(
        Permission, blank=True, verbose_name=_("Permissions granting access")
    )
    uuid = models.CharField(max_length=36, unique=True, default=uuid.uuid4())
    #    keysequence = models.CharField(max_length=20)

    def save(self, **kwargs):
        # Ensure that item is not its own parent, since this breaks
        # the site tree (and possibly the entire site).
        if self.parent == self:
            self.parent = None
        super().save(**kwargs)

    def __str__(self):
        """Cascade parents with '/'"""
        item = self
        s = item.title
        while item.parent:
            s = item.parent.title + "/" + s
            item = item.parent
        return s.replace("&", "")
