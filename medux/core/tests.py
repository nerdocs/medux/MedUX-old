"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.test import TestCase

# from medux.core.models import Coding
#
#
# class CodingTest(TestCase):
#     """Implements some simple tests for the 'Coding' model."""
#
#     def setUp(self):
#         Coding.objects.create(system="urn:uuid:53fefa32-fcbb-4fd8-8a91-553e120877b7",
#                               version='2.0',
#                               code='yes',
#                               display='YES',
#                               userselected=True)
#
#     def test_Coding(self):
#         self.assertEqual(Coding.objects.count(), 1)
#
#     def test_get_object(self):
#         coding = Coding.objects.filter(userselected=True).first()
#         self.assertEqual(coding.system, 'urn:uuid:53fefa32-fcbb-4fd8-8a91-553e120877b7')
#
