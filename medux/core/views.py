"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from rest_framework import generics, mixins

# from medux.core.models import HumanName
from medux.core.api.serializers import PatientSerializer
from medux.core.models import Patient


class PatientAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field = "pk"
    serializer_class = PatientSerializer

    def get_queryset(self):
        return Patient.objects.all()

    def perform_create(self, serializer):
        serializer.save()

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# class HumanNameAPIView(mixins.CreateModelMixin, generics.ListAPIView):
#    lookup_field = 'pk'
#    serializer_class = HumanNameSerializer
#
#    def get_queryset(self):
#        """
#        Looks for HumanNames in the database. An optional parameter "q" may be used to filter for given and family name.
#        """
#        qs = HumanName.objects.all()
#        query = self.request.GET.get('q')
#        if query is not None:
#            qs = qs.filter(Q(family__icontains=query) | Q(given__icontains=query)).distinct()
#        return qs
#
#    def perform_create(self, serializer):
#        serializer.save()
#
#    def post(self, request, *args, **kwargs):
#        return self.create(request, *args, **kwargs)


# class HumanNameRudView(generics.RetrieveUpdateDestroyAPIView):
#    lookup_field = 'pk'
#    serializer_class = HumanNameSerializer
#
#    def get_queryset(self):
#        return HumanName.objects.all()
#
#    def get_serializer_context(self, *args, **kwargs):
#        return {'request': self.request}
