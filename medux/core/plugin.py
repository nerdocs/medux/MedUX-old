from medux.api import IMeduxModule
from django.utils.translation import gettext_lazy as _


class CoreModule(IMeduxModule):
    app = "core"

    def setup(self):
        """Setup the Core module.

        It registers a few menus, menu items, etc."""

        mainmenu = self.register_menu(
            title=_("Main menu"),
            name="mainmenu",
            uid="634073cd-0f0f-4cc6-846c-35f036ae76c8",
        )

        # FIXME: should items be translated here?
        patient = self.register_menuitem(
            title=_("Patient"),
            slug="patient",
            icon="user",
            uid="b3afa405-ce99-4324-ad88-54c61d979708",
        )
        self.register_menuitem(
            title=_("&New"),
            parent=patient,
            slug="patient/new",
            uid="a9ee2cb2-eb6c-41cc-b0e3-89a9b6d4a0e7",
        )

    def initialize(self):
        pass

    def delayed_initialize(self):
        pass

    def activate(self):
        pass

    def deactivate(self):
        pass

    def teardown(self):
        pass
