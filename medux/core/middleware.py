"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging

from channels.middleware import BaseMiddleware
from django.conf import settings
from django.core.exceptions import MiddlewareNotUsed
from gdaps.pluginmanager import PluginManager

logger = logging.getLogger(__name__)


class PluginMiddleware(BaseMiddleware):
    def __init__(self, get_response):
        """One-time configuration and initialization of plugins."""
        #        PluginManager().reload_plugins()
        logger.info(
            "Loaded plugins: {}".format(
                []
                # [apps for apps in settings.PLUGINS \
                # if apps.startswith('medux')]
            )
        )
        raise MiddlewareNotUsed
