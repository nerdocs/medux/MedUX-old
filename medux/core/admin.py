"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered

from medux.core.models import Name, Identity
from medux.models import MeduxPlugin


class NamesInline(admin.TabularInline):
    model = Name


@admin.register(Identity)
class PatientAdmin(admin.ModelAdmin):
    inlines = [NamesInline]


for app_name in ["core"]:
    app_models = apps.get_app_config(app_name).get_models()
    for model in app_models:
        try:
            if not model._meta.abstract:
                admin.site.register(model)
        except AlreadyRegistered:
            pass
