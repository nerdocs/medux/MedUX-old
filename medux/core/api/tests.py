"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# from rest_framework.test import APITestCase
# from rest_framework.reverse import reverse as api_reverse
# from rest_framework import status
# from medux.core.models import HumanName
# from rest_framework_jwt.settings import api_settings
# from django.contrib.auth.models import User
#
# payload_handler = api_settings.JWT_PAYLOAD_HANDLER
# encode_handler = api_settings.JWT_ENCODE_HANDLER
#
#
# class HumanNameAPITestCase(APITestCase):
#     # Todo: periods are not tested yet
#     def setUp(self):
#         # initialize the DB with some testdata
#         HumanName.objects.create(use='usual', given='Max', family='Mustermann', prefix='DI(FH)')
#         HumanName.objects.create(use='official', given='Hanna', family='Musterfrau', suffix='BSc.')
#         HumanName.objects.create(use='maiden', given='Gustav', family='Maier')
#         HumanName.objects.create(use='usual', given='Hans', family='Shepard')
#
#         # Create a user to be able to make authorized requests
#         user = User(username='testuser', email='abc@def.gh')
#         user.set_password('secretpassword')
#         user.save()
#
#     def test_human_names(self):
#         # checks if the testdata has been created successfully
#         self.assertEqual(HumanName.objects.count(), 4)
#         self.assertEqual(HumanName.objects.filter(family__icontains='Muster').count(), 2)
#         self.assertEqual(User.objects.count(), 1)
#
#     def test_get_list(self):
#         data = {}
#         url = api_reverse('humanmame-api')
#         response = self.client.get(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#     def test_create_item(self):
#         data = {'use': 'usual', 'given': 'Jochen', 'family': 'Bauer', 'prefix': 'Dr.'}
#         url = api_reverse('humanmame-api')
#         response = self.client.post(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_get_item(self):
#         data = {}
#         hn = HumanName.objects.first()
#         url = api_reverse('humanmame-rud', kwargs={'pk': hn.pk})
#         response = self.client.get(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertEqual(response.data.get('family'), 'Mustermann')
#         self.assertEqual(response.data.get('given'), 'Max')
#
#     def test_update_item(self):
#         data = {'use': 'usual', 'given': 'Jochen', 'family': 'Bauer', 'prefix': 'Dr.'}
#         hn = HumanName.objects.first()
#         url = api_reverse('humanmame-rud', kwargs={'pk': hn.pk})
#         response = self.client.post(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#         response = self.client.put(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_update_item_authenticated(self):
#         data = {'use': 'usual', 'given': 'Jochen', 'family': 'Bauer', 'prefix': 'Dr.'}
#         hn = HumanName.objects.first()
#         url = api_reverse('humanmame-rud', kwargs={'pk': hn.pk})
#         user = User.objects.first()
#         payload = payload_handler(user)
#         token_rsp = encode_handler(payload)
#         self.client.credentials(HTTP_AUTHORIZATION='JWT %s' % token_rsp)  # JWT <token>
#
#         response = self.client.put(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#         response = self.client.get(url, {}, format='json')
#         for key in data:
#             self.assertEqual(data[key], response.data.get(key))
#
#     def test_create_item_authenticated(self):
#         data = {'use': 'nickname', 'given': 'Alfred', 'family': 'Weichselmaier', 'suffix': 'MA'}
#         url = api_reverse('humanmame-api')
#         user = User.objects.first()
#         payload = payload_handler(user)
#         token_rsp = encode_handler(payload)
#         self.client.credentials(HTTP_AUTHORIZATION='JWT %s' % token_rsp)  # JWT <token>
#         response = self.client.post(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         url = api_reverse('humanmame-rud', kwargs={'pk': HumanName.objects.last().pk})
#         response = self.client.get(url, {}, format='json')
#         for key in data:
#             self.assertEqual(data[key], response.data.get(key))
#
#         data = {}
#         url = api_reverse('humanmame-api')
#         response = self.client.post(url, data, format='json')
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
#
#     def test_delete_item(self):
#         hn = HumanName.objects.last()
#         url = api_reverse('humanmame-rud', kwargs={'pk': hn.pk})
#         response = self.client.delete(url, {}, format='json')
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#     def test_delete_item_authenticated(self):
#         hn = HumanName.objects.last()
#         hn_count = HumanName.objects.count()
#         url = api_reverse('humanmame-rud', kwargs={'pk': hn.pk})
#         user = User.objects.first()
#         payload = payload_handler(user)
#         token_rsp = encode_handler(payload)
#         self.client.credentials(HTTP_AUTHORIZATION='JWT %s' % token_rsp)  # JWT <token>
#         response = self.client.delete(url, {}, format='json')
#         self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
#         self.assertEqual(hn_count - 1, HumanName.objects.count())
