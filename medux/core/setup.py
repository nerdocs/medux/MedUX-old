from setuptools import setup

setup(
    name="medux.core",
    version="0.0.2",
    author="Christian González, Georg Schäfer",
    author_email="medux@nerdocs.at",
    description="MedUX core plugin",
    install_requires=["medux"],
    url="https://gitlab.com/nerdocs/medux/MedUX",
    namespace_packages=["medux"],
    packages=["medux.core"],
    license="AGPLv3+",
    summary="The Core of Medux. This module provides basic functionality for interacting with patients, etc.,",
    # See https://pypi.org/classifiers/
    classifiers=[
        "Development Status :: 1 - Planning",
        "Environment :: Console",
        "Environment :: Web Environment",
        "Framework :: Django",
        "Intended Audience :: Healthcare Industry",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Topic :: Scientific/Engineering :: Medical Science Apps.",
    ],
    entry_points={"medux.server.modules": ["core = medux.core:CoreModule"]},
)
