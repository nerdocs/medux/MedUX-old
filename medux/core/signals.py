"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

# from .models import HumanName
from medux.core.models import Patient
import channels.layers
from asgiref.sync import async_to_sync

from medux.models import MeduxPlugin

channel_layer = channels.layers.get_channel_layer()


@receiver(post_save, sender=Patient)
def model_update_handler(sender, instance, created, **kwargs):
    msg = dict()
    msg["status"] = "create" if created else "update"
    msg["resource_id"] = instance.pk
    async_to_sync(channel_layer.group_send)(
        "resource_{}".format(sender.__name__.lower()),
        {"type": "resource_message", "message": msg},
    )


@receiver(post_delete, sender=Patient)
def model_delete_handler(sender, instance, **kwargs):
    msg = dict()
    msg["resource_id"] = instance.pk
    msg["status"] = "delete"
    async_to_sync(channel_layer.group_send)(
        "resource_{}".format(sender.__name__.lower()),
        {"type": "resource_message", "message": msg},
    )


# Signals for PluginSystem
@receiver(post_save, sender=MeduxPlugin)
def model_initialize_handler(sender, instance, created, **kwargs):
    if created:
        instance.initialize()


@receiver(post_delete, sender=MeduxPlugin)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    instance.uninstall()
