import graphene
from gdaps.graphene.api import IGraphenePlugin
from graphene_django import DjangoObjectType

from medux.core.models import Patient, MenuItem
from medux.models import MeduxPlugin


# MeduxPlugins
class MeduxPluginType(DjangoObjectType):
    """A MedUX plugin"""

    class Meta:
        model = MeduxPlugin


class PluginQuery:
    plugins = graphene.List(MeduxPluginType)

    @staticmethod
    def resolve_plugins(self, info, **kwargs):
        return MeduxPlugin.objects.all()


class PluginObject(IGraphenePlugin):
    query = PluginQuery


# Patients
class PatientType(DjangoObjectType):
    """A basic representation of a patient"""

    class Meta:
        model = Patient


class PatientQuery:
    patients = graphene.List(PatientType)

    @staticmethod
    def resolve_patients(self, info, **kwargs):
        return Patient.objects.all()


class PatientObject(IGraphenePlugin):
    query = PatientQuery


# Menus
class MenuType(DjangoObjectType):
    """A menu, can be placed in different parts of the GUI"""

    class Meta:
        model = MenuItem
        filter_fields = ["app", "title", "slug"]


class MenuQuery:
    menus = graphene.List(MenuType)

    @staticmethod
    def resolve_menus(self, info, **kwargs):
        # TODO: restrict to current permissions
        return MenuItem.objects.filter(menu=True)


class MenuObject(IGraphenePlugin):
    query = MenuQuery


# MenuItems
class MenuItemType(DjangoObjectType):
    """A menu item: menu, submenu, item, separator"""

    class Meta:
        model = MenuItem


class MenuItemQuery:
    menu_items = graphene.List(MenuItemType)

    @staticmethod
    def resolve_menu_items(self, info, **kwargs):
        # TODO: restrict to current permissions
        return MenuItem.objects.filter(menu=False)


class MenuItemObject(IGraphenePlugin):
    query = MenuItemQuery
