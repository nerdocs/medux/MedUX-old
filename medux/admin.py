from django.contrib import admin

from medux.models import MeduxPlugin


@admin.register(MeduxPlugin)
class MeduxPluginAdmin(admin.ModelAdmin):
    pass
