from rest_framework import serializers
from medux.models import MeduxPlugin


class PluginSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeduxPlugin
        fields = ["name", "description"]
