"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import uuid

import yaml
from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from gdaps import Interface, PluginError
from medux.core.models import MenuItem

logger = logging.getLogger(__name__)

@Interface
class IMeduxModule:
    """Interface of a Medux Module

    MedUX consists of modules, each serving a clearly defined purpose.
    Every MedUX module must implement this class.
    See also AbstractBaseModule as easy starting class to inherit from.
    """
    app = None

    def setup(self):
        """Runs once, at installation of plugin.

        Implementations could e.g. set a flag for first-time notifications
        to the user if something substantial in this plugin version has
        changed.
        """

    def initialize(self):
        """Called at application start.

        Everything the model needs during runtime in memory should be loaded
        here. But try to keep it fast to reduce server start time."""

    def delayed_initialize(self):
        """
        Called after full start of the application, to set up other tasks
        that are not that important. Can be called async.
        """

    def activate(self):
        """Code that should run when the plugin is activated."""

    def deactivate(self):
        """Code that should run when the plugin is deactivated."""

    def teardown(self):
        """Called at module uninstall.

        This should ideally remove all traces from the database."""

    def plugin_spec(self) -> dict:
        """Returns a dict with the plugin specifications.

        this function per default loads a TOML file with the plugin
        specifications and returns a dict with the kes/value pairs.
        The following values must be part of the dict:
        * version: the version number of a module should follow the
        conventions of https://www.python.org/dev/peps/pep-0440 -
        MAJOR.MINOR.PATCH. This is similar to the the "semantic versioning
        of the semver python module.
        * vendor: the vendor of the module. Can be empty.
        * author: the author of the module, or a list of authors
        * author_email: the author's emails
        * core_version: the minimum version of MedUX Core this
        module requires to work. Should be a comma separated string like
        ">=1.0.3,<4.0.0"
        """



    def __init__(self):
        if not self.__class__.__name__.endswith("Module"):
            raise PluginError(
                "{}: Medux Modules must be called <Any-name>Module.".format(
                    self.__class__
                )
            )

        if not self.app:
            raise PluginError(
                "{} You have to set the 'app' attribute of a MedUX module.".format(
                    self.__class__
                )
            )
        self._spec = None

    @property
    def plugin_spec(self) -> dict:
        # FIXME: should load the plugin.yaml from the current plugin directory.
        if not self._spec:
            self._spec = yaml.safe_load("plugin.yaml")
        return self._spec

    def register_menu(
        self, title: str, name: str, uid: str = None, override: bool = False
    ):
        """
        Convenience function to add a menu.

        Internally calls register_menuitem with the given parameters
        :param title: The human readable title of the menu.
        :param name: the internal name of the menu.
        :param uid: (optional) see register_menuitem()
        :param override: (optional) see register_menuitem()
        :return: The menu item just created or fetched.
        """
        return self.register_menuitem(title=title, slug=name, uid=uid)

    def register_menuitem(
        self,
        title: str,
        slug: str,
        icon: str = None,
        parent: int or str = None,
        uid: uuid.UUID = None,
        override: bool = False,
        menu: bool = False,
    ) -> models.Model:
        """Registers a menu item globally.

        :param title: The (translatable) title of the menu item. Translation will be
            done on the fly.
        :param slug: the slug where the URL should point to. If it starts with a "/",
            the menu item is meant to be a menu, no menuitem.
        :param icon: a Material design icon name.
        :param parent: an int (pk) or str(uuid) of the menu item which this item should
            be a child.
        :param override: If true, update DB fields with the given values. If false,
            leave existing menu item as is. If it doesn't exist, it will be created.
        :param uid: a globally, predefined unique ID for this menu item. This could be
            referenced by other plugins to place menu items under a known other menu
            item. Raises an error if the uuid does not exist.
        :param menu: True if this menu item is a menu itself. Should not be used, use
            .. `register_menu()`_ instead. Defaults to False.
        :returns: Model item that was created/updated (which you then can pass as
            parent to another item).
        """

        if parent is not None:
            assert isinstance(parent, models.Model)

        if uid:
            try:
                # First look if MenuItem already exists.
                item = MenuItem.objects.get(uuid=uid)

                # Only write fields to DB again if explicitly told
                if override:
                    item.app = self.app
                    item.menu = menu
                    item.title = title
                    item.slug = slug
                    item.icon = icon
                    item.parent = parent
                    item.uuid = uid
                    item.save()
                    logger.debug("{}: Updated MenuItem {} in DB".format(self.app, slug))

            except ObjectDoesNotExist:
                item = MenuItem(
                    title=title,
                    slug=slug,
                    icon=icon,
                    app=self.app,
                    parent=parent,
                    uuid=uid,
                )
                item.save()
                logger.info("{}: Adding MenuItem {} to DB".format(self.app, slug))
        else:
            item = MenuItem(
                title=title, slug=slug, icon=icon, app=self.app, uuid=uuid.uuid4()
            )
            item.save()
            logger.debug("{}: Added new MenuItem {} to DB".format(self.app, slug))

        return item

    def setup(self):
        pass

    def initialize(self):
        pass

    def delayed_initialize(self):
        pass

    def activate(self):
        pass

    def deactivate(self):
        pass

    def teardown(self):
        pass
