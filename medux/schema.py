"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging

import graphene
import graphql_jwt
from gdaps.graphene.api import IGraphenePlugin

import medux.core.schema
from gdaps.graphene.schema import GDAPSQuery, GDAPSMutation
from graphene_django import DjangoObjectType

# import graphql_jwt

# This is MedUX' main Graphene schema file. It dynamically imports all
# plugins' schema.py files and loads their schemas too.


class JWTMutation(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


# needed for GraphQL JWT TokenAuth
class JWTObject(IGraphenePlugin):
    mutation = JWTMutation


logger = logging.getLogger(__name__)

schema = graphene.Schema(query=GDAPSQuery, mutation=GDAPSMutation)
