"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.core.exceptions import ValidationError
from django.test import TestCase

from gdaps.validators import validate_plugin_spec
import jsonschema


class PluginSpecValidatorTest(TestCase):
    def _get_dummy_spec(self):
        return {
            "Plugin": {
                "name": "test_hello3",
                "verbose_name": "Test Hello 3",
                "category": "Planetary",
                "version": "0.0.1",
                "dependencies": {"core": "0.0.1"},
                "author": "Me",
                "author_email": "no@example.com",
                "vendor": "MyVendor",
            }
        }

    def _test_wrong_spec(self, value: str):
        spec = self._get_dummy_spec()
        spec["Plugin"]["version"] = value
        # We are here a bit generic with "Exception", as many parts in the
        # validation process can fail and raise different exceptions
        with self.assertRaises(Exception):
            validate_plugin_spec(spec)

    def _test_correct_spec(self, field: str, value: str):
        spec = self._get_dummy_spec()
        spec["Plugin"][field] = value
        validate_plugin_spec(spec)

    def test_wrong_spec_version1(self):
        self._test_wrong_spec("abracadabra")

    def test_wrong_spec_version2(self):
        self._test_wrong_spec("%4§")

    def test_wrong_spec_version3(self):
        self._test_wrong_spec(">3.4=")

    def test_wrong_spec_version4(self):
        self._test_wrong_spec(">3.0,<5.0")

    def test_wrong_spec_version5(self):
        self._test_wrong_spec("01.2.3")

    def test_empty_spec_version1(self):
        self._test_wrong_spec("")

    def test_empty_spec_version2(self):
        self._test_wrong_spec(" ")

    def test_correct_spec1(self):
        self._test_correct_spec("version", "3.0.0")
        self._test_correct_spec("version", "10.0.0")
        self._test_correct_spec("version", "1440.23.1240")
