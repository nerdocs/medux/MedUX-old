"""
MedUX - A Free/OpenSource Electronic Medical Record
Copyright (C) 2017 Christian González

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from django.test import TestCase

from medux.settings import BASE_DIR
from gdaps.pluginmanager import PluginManager


class PluginLoadTest(TestCase):
    def setUp(self):
        self.plugin_manager = PluginManager()

    def _test_find_plugins(self, directory: str, expected_result: list):
        """calls PluginManager.find_plugins and checks
        if result is `expected`"""

        self.plugin_manager.set_plugin_dir(
            os.path.join(BASE_DIR, "medux", "tests", directory)
        )
        self.assertEqual(self.plugin_manager.find_plugins(), expected_result)

    def test_find_plugins_ok(self):
        self._test_find_plugins(
            "plugins_ok",
            [
                (
                    "medux.plugins.ok1",
                    {
                        "Plugin": {
                            "name": "ok1",
                            "app_name": "medux.plugins.ok1",
                            "verbose_name": "Test Hello3",
                            "category": "misc",
                            "version": "0.0.1",
                            "dependencies": {"core": "0.0.1"},
                            "author": "Firstname Lastname",
                            "author_email": "df@sdf.dsf",
                            "vendor": "test",
                        }
                    },
                )
            ],
        )

    def test_find_plugins_with_errors(self):
        self._test_find_plugins("plugins_with_errors", [])
