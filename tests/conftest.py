from django.conf import settings


def pytest_configure():
    settings.configure(
        DATABASES={"default": {"ENGINE": "django.db.backends.sqlite3"}},
        INSTALLED_APPS=[
            "django.contrib.admin",
            "django.contrib.auth",
            "django.contrib.contenttypes",
            "django.contrib.sessions",
            "django.contrib.messages",
            "django.contrib.staticfiles",
            # third party apps
            "rest_framework",
            "channels",
            "webpack_loader",
            "graphene_django",
            "gdaps",
            # local apps
            "medux.apps.MeduxConfig",
            "medux.core.apps.CoreConfig",
        ],
    )
