# for production
django
djangorestframework
djangorestframework-jwt
graphene_django
channels
channels_redis
django-webpack-loader
django-polymorphic
semver
django_graphql_jwt
PyYAML
django-statici18n
gdaps