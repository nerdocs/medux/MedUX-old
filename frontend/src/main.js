import Vue from 'vue'
import router from './router'
import { createProvider } from './vue-apollo'
import Vuetify  from 'vuetify'
import App from './App.vue'
import 'vuetify/dist/vuetify.min.css'
import VueRouter from 'vue-router'
import routes from './router/index'
import plugins from './plugins/plugins'

Vue.config.productionTip = false
Vue.use(Vuetify)


// const router = new VueRouter({
//   routes // short for `routes: routes`
// })

new Vue({
  router,
  apolloProvider: createProvider(),
  render: h => h(App)
}).$mount('#app')
